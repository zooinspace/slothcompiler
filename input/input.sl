//Мета-программирование
//execute(:(execute(:(println(:(Hi from first!))))), :(execute(:(println(:(Hi from second!))))))


function hi(to) {
	println(:(Hi, ) + to + :(!))
}
hi(:(Oleg1))

function sum(a, b) {
	return a + b
}
println(sum(10, 15))

// Это операции над числами

hexVariable = #3a
bitChainVariable = @11001
 
variableThatContainsAllArithmetic = ((2 + 2) ^ 2) / 4 * 3 - 50 + (-60) + $5
variableThatContainsAllSystemConstants = PI + G + E + CURRENT_TIME
variableThatContainsAllSpecialChars = :(\n) + :(\t)

// Это операции над строками
str1 = :(Oleg) * 7
str2 = :(Con) + :(catenation)
str3 = $:(Permutations)

print(:(HelloWorld!\n) + variableThatContainsAllSystemConstants + :(\n))

print(CURRENT_TIME + :(\n))

for i = 0, i < 10, i = i + 1 {
	print(:(working...\n))
}
print(CURRENT_TIME + :(\n))

if (1 >= 0) or (0 < 1) and (0 > 3) {
	print(:(Tree1\n))
	print(:(Tree2\n))
} else if (1 >= 0) {
	println(:(sdasd))
}

until (1 == 1) {
	println(:(until\n))
} else until(2 == 2) {
	println(:(else until))
} else {
	println(:(Some other else in until))
}

print(:(DoUntil Test\n))

i = 0
do {
	print(:(i = ) + i + :(\n))
	i = i + 1
	if i == 5 break
} while (i < 10)

j = 0

print(:(While Test\n))

while (j < 10) {
	j = j + 1
	print(:(j = ) + j + :(\n))
}

print(:(For Test\n))

for r = 0, r < 10, r = r + 1 {
	if r == 3 continue
	print(:(r = ) + r + :(\n))
}

println(:(TestFunc), :(Other Arg))
println(:(TestO), typeof(1) + typeof(:(Oleg)))

print(:(sin(PI) = ) + sin(PI / 2))

function input_test() {
	a = input()
	b = number(a)
	c = string(a) + :( - String)
	println(a)
	println(b)
	println(c)
}

function like_python() {
	println(number(input()) + number(input()))
}

function bitOperationsTest() {
	println(1 >> 2)
	println(1 << 2)
	println(1 >>> 2)
	println(-4 <<< 1)
	println(1 |! 2)
	println(~1)
}

function transformFunctions() {
	println(:(\nTr funs))
	println(hex(@0110))
	println(bits(#3a))
	println(number(:(1102))
	println(string(:(@10101)))
	println(integer(2.5))
}
transformFunctions()

function start() {
	
	a, b, c = 0, 1, 2
	
	println(a + :( ), b + :( ), c)
	
	function FakeObject() {
	
		Name = :(Fake OOP object)
		
		function yieldName() {
			print(Name)
		}
		
		yieldName()
		
	}
	
	FakeObject()
		
}

/*

object A {
	a = 3
	
	function hi() {
		
	}
}

*/