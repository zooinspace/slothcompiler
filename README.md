# README #

This document is to show how to use the compiler for programming language Sloth.

### Quick reference ###

* In this moment the language is developing actively.
* Current version is: "Vanilla"

### How do I get set up? ###

* Download [jre8](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) or higher.
* Use the Java command line interface: "java SlothCompiler.class <directory_of_source_file>"
* Or you can use our own [mini-IDE]() or try [Eclipse]() and [IntelliIDEA]() utilities. (Soon) 

### Who do I talk to? ###

* Oleg Bedrin, lead developer.  
** numeralhuman@gmail.com **