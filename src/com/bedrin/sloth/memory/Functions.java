package com.bedrin.sloth.memory;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

import com.bedrin.sloth.datatypes.ArrayValue;
import com.bedrin.sloth.datatypes.NumberValue;
import com.bedrin.sloth.datatypes.StringValue;
import com.bedrin.sloth.datatypes.VoidValue;
import com.bedrin.sloth.exceptions.MissingFunctionException;
import com.bedrin.sloth.exceptions.WrongNumberOfArgumentsException;
import com.bedrin.sloth.exceptions.WrongTypeOfArgumentsException;
import com.bedrin.sloth.parser.Lexer;
import com.bedrin.sloth.parser.Parser;
import com.bedrin.sloth.parser.interfaces.IFunction;
import com.bedrin.sloth.parser.interfaces.IValue;

public final class Functions {
	
	private Functions() {}
	
	private static Map<String, IFunction> functions;
	private static Stack<Map<String, IFunction>> stack;
	
	private static final String[] SYSTEM_FUNCTIONS = {
		"sin", // 0
		"cos", // 1
		"tan", // 2
		"asin", // 3
		"acos", // 4
		"atan", // 5
		"typeof", // 6
		"print", // 7
		"println", // 8
		"execute", // 9
		"number",  // 10
		"string", // 11
		"input", // 12
		"bits", // 13
		"hex", // 14
		"integer", // 15
		"array", // 16
		"length" // 17
	};
	
	public static void init() {
		stack = new Stack<Map<String, IFunction>>();
		functions = new HashMap<String, IFunction>();
		initSystemFunctions();
	}
	
	private static void initSystemFunctions() {
		functions.put(SYSTEM_FUNCTIONS[0], (IFunction) (IValue... args) -> {
			if(args.length != 1) {
				throw new WrongNumberOfArgumentsException(SYSTEM_FUNCTIONS[0], 1);
			} else {
				return new NumberValue(Math.sin(args[0].asDouble()));
			}
		});
		functions.put(SYSTEM_FUNCTIONS[1], (IFunction) (IValue... args) -> {
			if(args.length != 1) {
				throw new WrongNumberOfArgumentsException(SYSTEM_FUNCTIONS[1], 1);
			} else {
				return new NumberValue(Math.cos(args[0].asDouble()));
			}
		});
		functions.put(SYSTEM_FUNCTIONS[2], (IFunction) (IValue... args) -> {
			if(args.length != 1) {
				throw new WrongNumberOfArgumentsException(SYSTEM_FUNCTIONS[2], 1);
			} else {
				return new NumberValue(Math.tan(args[0].asDouble()));
			}
		});
		functions.put(SYSTEM_FUNCTIONS[3], (IFunction) (IValue... args) -> {
			if(args.length != 1) {
				throw new WrongNumberOfArgumentsException(SYSTEM_FUNCTIONS[3], 1);
			} else {
				return new NumberValue(Math.asin(args[0].asDouble()));
			}
		});
		functions.put(SYSTEM_FUNCTIONS[4], (IFunction) (IValue... args) -> {
			if(args.length != 1) {
				throw new WrongNumberOfArgumentsException(SYSTEM_FUNCTIONS[4], 1);
			} else {
				return new NumberValue(Math.acos(args[0].asDouble()));
			}
		});
		functions.put(SYSTEM_FUNCTIONS[5], (IFunction) (IValue... args) -> {
			if(args.length != 1) {
				throw new WrongNumberOfArgumentsException(SYSTEM_FUNCTIONS[5], 1);
			} else {
				return new NumberValue(Math.atan(args[0].asDouble()));
			}
		});
		functions.put(SYSTEM_FUNCTIONS[6], (IFunction) (IValue... args) -> {
			if(args.length != 1) {
				throw new WrongNumberOfArgumentsException(SYSTEM_FUNCTIONS[6], 1);
			} else {
				String result = "Not Defined Datatype.";
				if(args[0] instanceof StringValue) {
					result = "string";
				} else if(args[0] instanceof NumberValue) {
					result = "number";
				}
				return new StringValue(result);
			}
		});
		functions.put(SYSTEM_FUNCTIONS[7], (IFunction) (IValue... args) -> {
			for(IValue i : args) {
				System.out.print(i);
			}
			return new VoidValue();
		});
		functions.put(SYSTEM_FUNCTIONS[8], (IFunction) (IValue... args) -> {
			if(args.length == 0) {
				System.out.println();
			} else {
				for(IValue i : args) {
					System.out.println(i);
				}
			}
			return new VoidValue();
		});
		functions.put(SYSTEM_FUNCTIONS[9], (IFunction) (IValue... args) -> {
			//TODO Сделать чтобы на вход принимался массив символов
			boolean valid = true;
			for(IValue i : args) {
				if(!(i instanceof StringValue)) {
					valid = false;
					break;
				}
			}
			if(valid) {
				StringBuilder buffer = new StringBuilder();
				for(IValue i : args) {
					buffer.append(i.asString()).append("\n");
				}
				Variables.push();
				Functions.push();
				new Parser(new Lexer(buffer.toString()).tokenize()).parse().execute();
				Functions.pop();
				Variables.pop();
			} else {
				throw new WrongTypeOfArgumentsException(SYSTEM_FUNCTIONS[9],"string");
			}
			return new VoidValue();
		});
		functions.put(SYSTEM_FUNCTIONS[10], (IFunction) (IValue... args) -> {
			if(args.length != 1) {
				throw new WrongNumberOfArgumentsException(SYSTEM_FUNCTIONS[10], 1);
			}
			return new NumberValue(args[0].asDouble());
		});
		functions.put(SYSTEM_FUNCTIONS[11], (IFunction) (IValue... args) -> {
			if(args.length != 1) {
				throw new WrongNumberOfArgumentsException(SYSTEM_FUNCTIONS[11], 1);
			}
			return new StringValue(args[0].asString());
		});
		functions.put(SYSTEM_FUNCTIONS[12], (IFunction) (IValue... args) -> {
			if(args.length != 0) {
				throw new WrongNumberOfArgumentsException(SYSTEM_FUNCTIONS[12], 0);
			}
			return new StringValue(new Scanner(System.in).next());
		});
		functions.put(SYSTEM_FUNCTIONS[13], (IFunction) (IValue... args) -> {
			if(args.length != 1 && !(args[0] instanceof NumberValue)) {
				throw new WrongNumberOfArgumentsException("that work only with numbers called " + SYSTEM_FUNCTIONS[13], 1);
			}
			return new StringValue("@" + Long.toBinaryString((long) args[0].asDouble()));
		});
		functions.put(SYSTEM_FUNCTIONS[14], (IFunction) (IValue... args) -> {
			if(args.length != 1 && !(args[0] instanceof NumberValue)) {
				throw new WrongNumberOfArgumentsException("that work only with numbers called " + SYSTEM_FUNCTIONS[14], 1);
			}
			return new StringValue("#" + Long.toHexString((long) args[0].asDouble()));
		});
		functions.put(SYSTEM_FUNCTIONS[15], (IFunction) (IValue... args) -> {
			if(args.length != 1 && !(args[0] instanceof NumberValue)) {
				throw new WrongNumberOfArgumentsException("that work only with numbers called " + SYSTEM_FUNCTIONS[15], 1);
			}
			return new NumberValue((long) args[0].asDouble());
		});
		functions.put(SYSTEM_FUNCTIONS[16], new IFunction() {

			@Override
			public IValue execute(IValue... args) {
				return createArray(args, 0);
			}
			
			private ArrayValue createArray(IValue[] args, int index) {
				int size = (int) args[index].asDouble();
				int last = args.length - 1;
				ArrayValue array = new ArrayValue(size);
				if(index == last) {
					for(int i = 0; i < size; i++) {
						array.set(i, new VoidValue());
					}
				} else if(index < last) {
					for(int i = 0; i < size; i++) {
						array.set(i, createArray(args, index + 1));
					}
				}
				return array;
			}
			
		});
		functions.put(SYSTEM_FUNCTIONS[17], (IFunction) (IValue... args) -> {
			if(args.length != 1) {
				throw new WrongNumberOfArgumentsException(SYSTEM_FUNCTIONS[17], 1);
			}
			if(args[0] instanceof ArrayValue) {
				return new NumberValue(((ArrayValue) args[0]).getLength());
			} else if(args[0] instanceof StringValue) {
				return new NumberValue(((StringValue) args[0]).getSize());
			}
			throw new WrongTypeOfArgumentsException(SYSTEM_FUNCTIONS[17], "array or string");
		});
	}
	
	public static void flush() { 
		functions.clear();
		stack.clear();
		initSystemFunctions();
	}
	
	public static void push() {
		stack.push(new HashMap<String, IFunction>(functions));
	}
	
	public static void pop() {
		functions = stack.pop();
	}
	
	public static boolean isExist(String key) {
		return functions.containsKey(key);
	}
	
	public static IFunction get(String key) {
		if(!isExist(key)) {
			throw new MissingFunctionException(key);
		}
		IFunction r = functions.get(key);
		return r;
	}
	
	public static void set(String key, IFunction iFunction) {
		functions.put(key, iFunction);
	}
	
}
