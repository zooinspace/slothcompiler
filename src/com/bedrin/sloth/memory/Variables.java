package com.bedrin.sloth.memory;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import com.bedrin.sloth.datatypes.NumberValue;
import com.bedrin.sloth.exceptions.MissingVariableException;
import com.bedrin.sloth.parser.interfaces.IValue;

public final class Variables {
	
	private Variables() {}
	
	private static final String[] SYSTEM_CONSTANTS = {
		"PI", "E", "G", "CURRENT_TIME"	
	};
	
	private static Map<String, IValue> variables;
	private static Stack<Map<String, IValue>> stack;
	
	public static void init() {
		stack = new Stack<Map<String, IValue>>();
		variables = new HashMap<String, IValue>();
	}

	private static void initSystemVariables() {
		variables.put(SYSTEM_CONSTANTS[0], new NumberValue(Math.PI));
		variables.put(SYSTEM_CONSTANTS[1], new NumberValue(Math.E));
		variables.put(SYSTEM_CONSTANTS[2], new NumberValue(9.18));
		variables.put(SYSTEM_CONSTANTS[3], new NumberValue(System.currentTimeMillis()));
	}
	
	public static void flush() {
		variables.clear();
		stack.clear();
		initSystemVariables();
	}
	
	public static void push() {
		stack.push(new HashMap<String, IValue>(variables));
	}
	
	public static void pop() {
		variables = stack.pop();
	}
	
	public static boolean isExist(String key) {
		boolean r = variables.containsKey(key);
		return r;
	}
	
	public static IValue get(String key) {
		if(!isExist(key)) {
			throw new MissingVariableException(key);
		}
		if(key.equals(SYSTEM_CONSTANTS[3])) {
			variables.put(SYSTEM_CONSTANTS[3], new NumberValue(System.currentTimeMillis()));
		}
		IValue r = variables.get(key);
		return r;
	}
	
	public static void set(String key, IValue iValue) {
		variables.put(key, iValue);
	}
	
}
