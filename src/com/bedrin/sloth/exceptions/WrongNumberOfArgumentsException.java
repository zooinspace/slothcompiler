package com.bedrin.sloth.exceptions;

public class WrongNumberOfArgumentsException extends RuntimeException {

	private static final long serialVersionUID = 7495358336962381761L;

	public WrongNumberOfArgumentsException(String name, long number) {
		super("The function " + name + "() has " + number + " arguments.");
	}
	
}
