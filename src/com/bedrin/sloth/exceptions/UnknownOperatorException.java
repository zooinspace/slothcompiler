package com.bedrin.sloth.exceptions;

public class UnknownOperatorException extends RuntimeException {

	private static final long serialVersionUID = -5476996314522068335L;

	public UnknownOperatorException() {
		super("Unknown operator!");
	}
	
}
