package com.bedrin.sloth.exceptions;

public class CastToNumberException extends RuntimeException {

	private static final long serialVersionUID = 1828245502349153170L;

	public CastToNumberException() {
		super("Cannot cast this type to number!");
	}
	
}
