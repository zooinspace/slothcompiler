package com.bedrin.sloth.exceptions;

public class WrongEscapeStringException extends RuntimeException {

	private static final long serialVersionUID = 1739177176617956799L;

	public WrongEscapeStringException() {
		super("Wrong escape string!");
	}
	
}
