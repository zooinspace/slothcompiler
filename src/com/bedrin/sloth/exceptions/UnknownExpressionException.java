package com.bedrin.sloth.exceptions;

public class UnknownExpressionException extends RuntimeException {

	private static final long serialVersionUID = 5158263012149479342L;

	public UnknownExpressionException() {
		super("Unknown expression!");
	}
	
}
