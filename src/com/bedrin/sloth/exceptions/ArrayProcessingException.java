package com.bedrin.sloth.exceptions;

public class ArrayProcessingException extends RuntimeException {

	private static final long serialVersionUID = 1056077760784120439L;

	public ArrayProcessingException() {
		super("Error during array processing exception!");
	}
	
}
