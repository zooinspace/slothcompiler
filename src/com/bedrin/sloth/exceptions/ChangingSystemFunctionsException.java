package com.bedrin.sloth.exceptions;

public class ChangingSystemFunctionsException extends RuntimeException {
	
	private static final long serialVersionUID = -7207345707082541291L;

	public ChangingSystemFunctionsException(String function) {
		super("The " + function + "() function is immutable!");
	}
	
}
