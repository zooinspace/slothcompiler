package com.bedrin.sloth.exceptions;

public class MissingVariableException extends RuntimeException {

	private static final long serialVersionUID = 3094749631709489443L;

	public MissingVariableException(String variable) {
		super("The variable '" + variable + "' does not exist!");
	}
	
}
