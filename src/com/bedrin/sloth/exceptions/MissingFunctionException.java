package com.bedrin.sloth.exceptions;

public class MissingFunctionException extends RuntimeException {

	private static final long serialVersionUID = -2003780609003469005L;

	public MissingFunctionException(String function) {
		super("The function '" + function + "' does not exist!");
	}
	
}
