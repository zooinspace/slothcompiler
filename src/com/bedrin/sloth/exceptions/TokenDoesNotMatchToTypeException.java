package com.bedrin.sloth.exceptions;

import com.bedrin.sloth.parser.Token;
import com.bedrin.sloth.parser.TokenType;

public class TokenDoesNotMatchToTypeException extends RuntimeException {

	private static final long serialVersionUID = 4679702007193835719L;

	public TokenDoesNotMatchToTypeException(Token token, TokenType type) {
		super("Token " + token + " does not match to type " + type + "!");
	}
	
}
