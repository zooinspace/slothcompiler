package com.bedrin.sloth.exceptions;

public class WrongComparisonException extends RuntimeException {

	private static final long serialVersionUID = 5281358209844728327L;

	public WrongComparisonException(String type1, String type2) {
		super(type1 + " is not comparable to " + type2);
	}
	
}
