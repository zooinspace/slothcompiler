package com.bedrin.sloth.exceptions;

public class UnknownStatementException extends RuntimeException {

	private static final long serialVersionUID = 3520498775398156742L;

	public UnknownStatementException() {
		super("Unknown statement!");
	}
	
}
