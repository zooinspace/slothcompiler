package com.bedrin.sloth.exceptions;

public class WrongStringDeclarationException extends RuntimeException {

	private static final long serialVersionUID = -6615595168407832073L;

	public WrongStringDeclarationException() {
		super("Wrong string declaration!");
	}
	
}
