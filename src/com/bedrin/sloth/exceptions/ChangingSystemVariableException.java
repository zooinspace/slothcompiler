package com.bedrin.sloth.exceptions;

public class ChangingSystemVariableException extends RuntimeException {
	
	private static final long serialVersionUID = -4903954761631121024L;

	public ChangingSystemVariableException(String variable) {
		super("The '" + variable + "' variable is constant!");
	}

}
