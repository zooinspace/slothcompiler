package com.bedrin.sloth.exceptions;

public class WrongTypeOfArgumentsException extends RuntimeException {

	private static final long serialVersionUID = -2267840075286037521L;
	
	public WrongTypeOfArgumentsException(String name, String type) {
		super("The " + name + "() function can only work with " + type + " arguments.");
	}
	
}
