package com.bedrin.sloth.exceptions;

public class WrongArrayCallException extends RuntimeException {

	private static final long serialVersionUID = -8937880853115861710L;

	public WrongArrayCallException() {
		super("Operator [] can be used only with arrays!");
	}
	
}
