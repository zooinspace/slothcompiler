package com.bedrin.sloth.datatypes;

import java.util.Arrays;

import com.bedrin.sloth.exceptions.CastToNumberException;
import com.bedrin.sloth.parser.interfaces.IValue;

public class ArrayValue implements IValue {

	private IValue[] content;
	
	public ArrayValue(int size) {
		this.content = new IValue[size];
	}
	
	public ArrayValue(IValue[] content) {
		this.content = new IValue[content.length];
		System.arraycopy(content, 0, this.content, 0, this.content.length);
	}
	
	public int getLength() {
		return this.content.length;
	}
	
	public IValue get(int i) {
		return this.content[i];
	}
	
	public void set(int i, IValue value) {
		this.content[i] = value;
	}
	
	public ArrayValue(ArrayValue array) {
		this(array.content);
	}
	
	@Override
	public double asDouble() {
		throw new CastToNumberException();
	}

	@Override
	public String asString() {
		return Arrays.toString(this.content);
	}
	
	@Override
	public String toString() {
		return asString();
	}

}
