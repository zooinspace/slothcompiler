package com.bedrin.sloth.datatypes;

import java.util.List;

import com.bedrin.sloth.parser.ast.statements.ReturnStatement;
import com.bedrin.sloth.parser.interfaces.IFunction;
import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IValue;

public class UserDefinedFunction implements IFunction {

	private List<String> argList;
	private IStatement body;
	
	public UserDefinedFunction(List<String> argList, IStatement body) {
		this.argList = argList;
		this.body = body;
	}
	
	public int getArgsCount() {
		return this.argList.size();
	}
	
	public String getArgNameByIndex(int i) {
		if(i < 0 || i >= getArgsCount()) {
			return "";
		}
		return this.argList.get(i);
	}
	
	@Override
	public IValue execute(IValue... args) {
		try {
			this.body.execute();
			return new VoidValue();
		} catch (ReturnStatement r) {
			return r.getResult();
		}
	}

}
