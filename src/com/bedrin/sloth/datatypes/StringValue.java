package com.bedrin.sloth.datatypes;

import com.bedrin.sloth.parser.interfaces.IValue;

public class StringValue implements IValue {

	private String value;
	
	public StringValue(String value) {
		this.value = value;
	}
	
	@Override
	public double asDouble() {
		try {
			return Double.parseDouble(value);
		} catch(NumberFormatException e) {
			return 0;
		} 
	}

	public int getSize() {
		return this.value.length();
	}
	
	@Override
	public String asString() {
		return value;
	}
	
	@Override
	public String toString() {
		return asString();
	}

}
