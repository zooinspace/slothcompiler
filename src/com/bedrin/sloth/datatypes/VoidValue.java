package com.bedrin.sloth.datatypes;

import com.bedrin.sloth.parser.interfaces.IValue;

public class VoidValue implements IValue {

	@Override
	public double asDouble() {
		return 0;
	}

	@Override
	public String asString() {
		return "";
	}

	@Override
	public String toString() {
		return "";
	}
}
