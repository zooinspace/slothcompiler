package com.bedrin.sloth;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import com.bedrin.sloth.datatypes.NumberValue;
import com.bedrin.sloth.datatypes.StringValue;
import com.bedrin.sloth.memory.Functions;
import com.bedrin.sloth.memory.Variables;
import com.bedrin.sloth.parser.Lexer;
import com.bedrin.sloth.parser.Parser;
import com.bedrin.sloth.parser.Token;
import com.bedrin.sloth.parser.interfaces.IFunction;
import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.visitors.Composer;

public class Sloth {

	///
	private static final boolean DEBUG = true;
	private static final boolean SHOW_TIME = true;
	///
	
	public static final String version = "Vanilla";
	
	private Sloth() {}
	
	static {
		Functions.init();
		Variables.init();
	}
	
	public static void addFunction(String name, IFunction f) {
		Functions.set(name, f);
	}
	
	public static void addStringVariable(String name, String v) {
		Variables.set(name, new StringValue(v));
	}
	
	public static void addNumberVariable(String name, double v) {
		Variables.set(name, new NumberValue(v));
	}
	
	public static String getStringVariable(String name) {
		return Variables.get(name).asString();
	}
	
	public static double getNumberVariable(String name) {
		return Variables.get(name).asDouble();
	}
	
	public static void interpretate(String source, String encoding) {
		String input = null;
		try {
			input = new String(Files.readAllBytes(Paths.get(source)), encoding);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		Lexer lex = new Lexer(input);
		List<Token> tks = lex.tokenize();
		
		if(DEBUG) {
			System.out.println("===TOKENS===");
			for(Token t : tks) {
				System.out.println(t.toString());
			}
			System.out.println("===TOKENS===");
		}
		
		IStatement program = new Parser(tks).parse();
		
		if (DEBUG) {
			System.out.println("===The Listing of program===");
			System.out.println(program.toString());
			System.out.println("===The Listing of program===");
			System.out.println("===The work of program===");
		}

		double millsOfWork;
		if(SHOW_TIME) {
			millsOfWork = System.currentTimeMillis();
		}
		
		program.accept(new Composer());
		program.execute();
		
		if (SHOW_TIME) {
			millsOfWork = System.currentTimeMillis() - millsOfWork;
			System.out.print("\nExecution time: " + (millsOfWork / 1000) + " sec");
		}
		
		if (DEBUG) {
			System.out.print("\n===The work of program===\n");
		}
	}

	public static void flushMemory() {
		Functions.flush();
		Variables.flush();
	}
	
	public static void interpretate(String source) {
		List<Token> tks = new Lexer(source).tokenize();
		IStatement program = new Parser(tks).parse();
		program.accept(new Composer());
		program.execute();
	}
	
}
