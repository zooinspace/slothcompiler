package com.bedrin.sloth.visitors;

import com.bedrin.sloth.parser.ast.expressions.ArrayAccessExpression;
import com.bedrin.sloth.parser.ast.expressions.ArrayExpression;
import com.bedrin.sloth.parser.ast.expressions.BinaryExpression;
import com.bedrin.sloth.parser.ast.expressions.ConditionalExpression;
import com.bedrin.sloth.parser.ast.expressions.FunctionalExpression;
import com.bedrin.sloth.parser.ast.expressions.UnaryExpression;
import com.bedrin.sloth.parser.ast.expressions.ValueExpression;
import com.bedrin.sloth.parser.ast.expressions.VariableExpression;
import com.bedrin.sloth.parser.ast.statements.ArrayAssignmentStatement;
import com.bedrin.sloth.parser.ast.statements.AssignmentStatement;
import com.bedrin.sloth.parser.ast.statements.BlockStatement;
import com.bedrin.sloth.parser.ast.statements.BreakStatement;
import com.bedrin.sloth.parser.ast.statements.ContinueStatement;
import com.bedrin.sloth.parser.ast.statements.DoWhileStatement;
import com.bedrin.sloth.parser.ast.statements.ForStatement;
import com.bedrin.sloth.parser.ast.statements.FunctionDefineStatement;
import com.bedrin.sloth.parser.ast.statements.FunctionalStatement;
import com.bedrin.sloth.parser.ast.statements.IfStatement;
import com.bedrin.sloth.parser.ast.statements.ReturnStatement;
import com.bedrin.sloth.parser.ast.statements.UntilStatement;
import com.bedrin.sloth.parser.ast.statements.WhileStatement;
import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public abstract class Visitor implements IVisitor {
	@Override
	public void visit(AssignmentStatement st) {
		st.getExpression().accept(this);
	}

	@Override
	public void visit(FunctionDefineStatement st) {
		st.getBody().accept(this);
	}

	@Override
	public void visit(FunctionalStatement st) {
		st.getFunction().accept(this);
	}

	@Override
	public void visit(ArrayAccessExpression arrayAccessExpression) {
		for (IExpression index : arrayAccessExpression.getIndicies()) {
			index.accept(this);
		}
	}

	@Override
	public void visit(ArrayExpression arrayExpression) {
		for (IExpression element : arrayExpression.getElements()) {
			element.accept(this);
		}
	}

	@Override
	public void visit(BinaryExpression binaryExpression) {
		binaryExpression.getFirst().accept(this);
		binaryExpression.getSecond().accept(this);
	}

	@Override
	public void visit(ConditionalExpression conditionalExpression) {
		conditionalExpression.getFirst().accept(this);
		conditionalExpression.getSecond().accept(this);
	}

	@Override
	public void visit(FunctionalExpression functionalExpression) {
		for (IExpression argument : functionalExpression.getArguments()) {
			argument.accept(this);
		}
	}

	@Override
	public void visit(UnaryExpression unaryExpression) {
		unaryExpression.getFirst().accept(this);
	}

	@Override
	public void visit(ValueExpression valueExpression) {
		
	}

	@Override
	public void visit(VariableExpression variableExpression) {
		
	}

	@Override
	public void visit(ArrayAssignmentStatement arrayAssignmentStatement) {
		arrayAssignmentStatement.getArray().accept(this);
		arrayAssignmentStatement.getExpression().accept(this);
	}

	@Override
	public void visit(BlockStatement blockStatement) {
		for (IStatement statement : blockStatement.getStatements()) {
			statement.accept(this);
		}
	}

	@Override
	public void visit(BreakStatement breakStatement) {
		
	}

	@Override
	public void visit(ContinueStatement continueStatement) {
		
	}

	@Override
	public void visit(DoWhileStatement doWhileStatement) {
		doWhileStatement.getCondition().accept(this);
		doWhileStatement.getStatement().accept(this);
	}

	@Override
	public void visit(ForStatement forStatement) {
		forStatement.getInitialization().accept(this);
		forStatement.getDecIncrementation().accept(this);
		forStatement.getTermination().accept(this);
		forStatement.getStatement().accept(this);
	}

	@Override
	public void visit(IfStatement ifStatement) {
		ifStatement.getExpression().accept(this);
		ifStatement.getIfStatement().accept(this);
		ifStatement.getElseStatement().accept(this);
	}

	@Override
	public void visit(ReturnStatement returnStatement) {
		
	}

	@Override
	public void visit(UntilStatement untilStatement) {
		untilStatement.getExpression().accept(this);
		untilStatement.getUntilStatement().accept(this);
		untilStatement.getElseStatement().accept(this);
	}

	@Override
	public void visit(WhileStatement whileStatement) {
		whileStatement.getCondition().accept(this);
		whileStatement.getStatement().accept(this);
	}

}
