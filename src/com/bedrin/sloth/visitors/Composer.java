package com.bedrin.sloth.visitors;

import com.bedrin.sloth.exceptions.ChangingSystemFunctionsException;
import com.bedrin.sloth.exceptions.ChangingSystemVariableException;
import com.bedrin.sloth.memory.Functions;
import com.bedrin.sloth.memory.Variables;
import com.bedrin.sloth.parser.ast.statements.AssignmentStatement;
import com.bedrin.sloth.parser.ast.statements.FunctionDefineStatement;

public class Composer extends Visitor {
	
	@Override
	public void visit(FunctionDefineStatement st) {
		super.visit(st);
		String t = st.getName();
		if(Functions.isExist(t)) {
			throw new ChangingSystemFunctionsException(t);
		}
		st.execute();
	}
	
	@Override
	public void visit(AssignmentStatement st) {
		super.visit(st);
		String t = st.getVariable();
		if(Variables.isExist(t)) {
			throw new ChangingSystemVariableException(t);
		}
	}

	
}