package com.bedrin.sloth.tests;

import java.io.File;

import com.bedrin.sloth.Sloth;

public class DebugEnter {
	
	private static final boolean IN_TEST = true;
	
	public static void main(String[] args) {
		Sloth.flushMemory();
		Sloth.interpretate("input" + File.separator + (IN_TEST ? "test_" : "") + "input.sl", "UTF-8");
		//Sloth.addNumberVariable("k", 10);
		//Sloth.interpretate("k = k + 1");
		//System.out.println(Sloth.getNumberVariable("k"));
		//Sloth.getNumberVariable("");
	}

}
