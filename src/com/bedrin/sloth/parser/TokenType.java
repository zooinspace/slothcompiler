package com.bedrin.sloth.parser;

public enum TokenType {
	
	
	NUMBER,
	WORD,
	HEX_NUMBER,
	BIT_CHAIN,
	TEXT,
	
	//keywords
	IF,
	ELSE,
	WHILE,
	FOR,
	DO,
	UNTIL,
	BREAK,
	CONTINUE,
	FUNCTION,
	RETURN,
	AND,
	OR,
	
	SLASH,
	STAR,
	MINUS,
	PLUS,
	FACT,
	EQ,
	EQEQ,
	LT,
	LTEQ,
	GT,
	GTEQ,
	EXCL,
	EXCLEQ,
	POWER,
	COMMA,
	
	BAR,
	AMP,
	
	LPAREN,
	RPAREN,
	QLPAREN,
	QRPAREN,
	FLPAREN,
	FRPAREN,
	
	RTRT, 
	LTLT, 
	RTRTRT, 
	LTLTLT, 
	BAREXCL,
	INVERSION,
	
	EOF;
	
}
