package com.bedrin.sloth.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Lexer {

	private static final String OPERATORS_CHARS = "+-*/$(){}=<>[]^&|,!~";
	
	private static Map<String, TokenType> OPERATORS;
	
	private String input;
	private List<Token> tokens;
	private final int inputLength;
	private int absolutePosition;
	
	public Lexer(String input) {
		this.input = input;
		this.inputLength = input.length();
		this.tokens = new ArrayList<Token>();
		initOperators();
	}

	public List<Token> tokenize() {
		while(this.absolutePosition < this.inputLength) {
			final char current = peek(0);
			if(Character.isDigit(current)) {
				tokenizeNumber();
			} else if(Character.isLetter(current)){
				tokenizeWord();
			} else if(current == '#') {
				next();
				tokenizeHexNumber();
			} else if(current == '@') {
				next();
				tokenizeBitChain(); 
			} else if(current == '"') {
				tokenizeText();
			}  else if(OPERATORS_CHARS.indexOf(current) != -1) {
				tokenizeOperator();
			} else {
				next();
			}
		}
		return tokens;
	}

	private void tokenizeText() {
		next();
		StringBuilder buffer = new StringBuilder();
		char current = peek(0);
		while (true) {
			if (current == '\\') {
				current = next();
				switch (current) {
				case 'n':
					current = next();
					buffer.append("\n");
					continue;
				case 't':
					current = next();
					buffer.append("\t");
					continue;
				default:
					current = next();
					buffer.append("\\");
					continue;
				}
			}
			if (current == '"') {
				break;
			} else {
				buffer.append(current);
				current = next();
			}
		}
		next();
		addToken(TokenType.TEXT, buffer.toString());
	}
	
	private void tokenizeWord() {
		StringBuilder buffer = new StringBuilder();
		char current = peek(0);
		while(true) {
			if(!Character.isLetterOrDigit(current) && (current != '_') && (current != ':')) {
				break;
			}
			buffer.append(current);
			current = next();
		}
		
		final String buff = buffer.toString();
		
		switch (buff) {
		
			case "if":
				addToken(TokenType.IF);
				break;
			
			case "else":
				addToken(TokenType.ELSE);
				break;
				
			case "while":
				addToken(TokenType.WHILE);
				break;
				
			case "for":
				addToken(TokenType.FOR);
				break;
			
			case "do":
				addToken(TokenType.DO);
				break;
			
			case "until":
				addToken(TokenType.UNTIL);
				break;
				
			case "break":
				addToken(TokenType.BREAK);
				break;
				
			case "continue":
				addToken(TokenType.CONTINUE);
				break;
			
			case "function":
				addToken(TokenType.FUNCTION);
				break;
				
			case "return":
				addToken(TokenType.RETURN);
				break;
			
			case "and":
				addToken(TokenType.AND);
				break;
			
			case "or":
				addToken(TokenType.OR);
				break;
				
			default:
				addToken(TokenType.WORD, buff);
				break;
		}
	}

	private void tokenizeBitChain() {
		StringBuilder buffer = new StringBuilder();
		char current = peek(0);
		while(Character.isDigit(current) && isBitChain(current)) {
			buffer.append(current);
			current = next();
		}
		addToken(TokenType.BIT_CHAIN, buffer.toString());
	}

	private void tokenizeHexNumber() {
		StringBuilder buffer = new StringBuilder();
		char current = peek(0);
		while(Character.isDigit(current) || isHex(current)) {
			buffer.append(current);
			current = next();
		}
		addToken(TokenType.HEX_NUMBER, buffer.toString());
	}
	
	private void tokenizeOperator() {
		char current = peek(0);
		if(current == '/') {
			if(peek(1) == '/') {
				next();
				next();
				tokenizeComment();
				return;
			} else if (peek(1) == '*') {
				next();
				next();
				tokenizeMultilineComment();
				return;
			}
		}
		StringBuilder buffer = new StringBuilder();
		while(true) {
			String text = buffer.toString();
			if(!OPERATORS.containsKey(text + current) && !text.isEmpty()) {
				addToken(OPERATORS.get(text));
				return;
			}
			buffer.append(current);
			current = next();
		}
	}

	private void tokenizeMultilineComment() {
		char current = peek(0);
		while(true) {
			if(current == '\0') {
				throw new RuntimeException("Missing close keyword for multiline comment!");
			}
			if(current == '*' && peek(1) == '/') {
				break;
			}
			current = next();
		}
		next(); // next for *
		next(); // next for /
	}

	private void tokenizeComment() {
		char current = peek(0);
		while("\0\r\n".indexOf(current) == -1) {
			current = next();
		}
	}

	private void tokenizeNumber() {
		StringBuilder buffer = new StringBuilder();
		char current = peek(0);
		while(true) {
			if(current == '.') {
				if(buffer.indexOf(".") != -1) {
					throw new RuntimeException("Invalid float number!");
				}
			} else if(!Character.isDigit(current)) {
				break;
			}
			buffer.append(current);
			current = next();
		}
		addToken(TokenType.NUMBER, buffer.toString());
	}

	private boolean isHex(char ch) {
		return "abcdef".indexOf(Character.toLowerCase(ch)) != -1;
	}
	
	private boolean isBitChain(char ch) {
		return "23456789".indexOf(ch) == -1 && !isHex(ch);
	}
	
	private char next() {
		this.absolutePosition++;
		return peek(0);
	}
	
	private char peek(int relativePosition) {
		final int pos = this.absolutePosition + relativePosition;
		if(pos >= this.inputLength) {
			return '\0';
		}
		return this.input.charAt(pos);
	}

	public String getInput() {
		return input;
	}
	
	public void addToken(TokenType type) {
		addToken(type, "");
	}
	
	public void addToken(TokenType type, String content) {
		tokens.add(new Token(content, type));
	}
	
	private void initOperators() {
		OPERATORS = new HashMap<String, TokenType>();
		OPERATORS.put("+", TokenType.PLUS);
		OPERATORS.put("-", TokenType.MINUS);
		OPERATORS.put("*", TokenType.STAR);
		OPERATORS.put("/", TokenType.SLASH);
		OPERATORS.put("$", TokenType.FACT);
		OPERATORS.put("=", TokenType.EQ);
		OPERATORS.put("^", TokenType.POWER);
		
		OPERATORS.put("(", TokenType.LPAREN);
		OPERATORS.put(")", TokenType.RPAREN);
		OPERATORS.put("[", TokenType.QLPAREN);
		OPERATORS.put("]", TokenType.QRPAREN);
		OPERATORS.put("{", TokenType.FLPAREN);
		OPERATORS.put("}", TokenType.FRPAREN);
		OPERATORS.put(",", TokenType.COMMA);
		
		OPERATORS.put(">>", TokenType.RTRT);
		OPERATORS.put("<<", TokenType.LTLT);
		OPERATORS.put(">>>", TokenType.RTRTRT);
		OPERATORS.put("<<<", TokenType.LTLTLT);
		OPERATORS.put("|!", TokenType.BAREXCL);
		OPERATORS.put("~", TokenType.INVERSION);
		
		OPERATORS.put("<", TokenType.LT);
		OPERATORS.put(">", TokenType.GT);
		OPERATORS.put("!", TokenType.EXCL);
		OPERATORS.put("!=", TokenType.EXCLEQ);
		OPERATORS.put("<=", TokenType.LTEQ);
		OPERATORS.put(">=", TokenType.GTEQ);
		OPERATORS.put("==", TokenType.EQEQ);
		OPERATORS.put("|", TokenType.BAR);
		OPERATORS.put("&", TokenType.AMP);
	}
	
}
