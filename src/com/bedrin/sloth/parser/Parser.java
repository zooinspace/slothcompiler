package com.bedrin.sloth.parser;

import java.util.ArrayList;
import java.util.List;

import com.bedrin.sloth.exceptions.TokenDoesNotMatchToTypeException;
import com.bedrin.sloth.exceptions.UnknownExpressionException;
import com.bedrin.sloth.exceptions.UnknownOperatorException;
import com.bedrin.sloth.parser.ast.expressions.ArrayAccessExpression;
import com.bedrin.sloth.parser.ast.expressions.ArrayExpression;
import com.bedrin.sloth.parser.ast.expressions.BinaryExpression;
import com.bedrin.sloth.parser.ast.expressions.ConditionalExpression;
import com.bedrin.sloth.parser.ast.expressions.FunctionalExpression;
import com.bedrin.sloth.parser.ast.expressions.UnaryExpression;
import com.bedrin.sloth.parser.ast.expressions.ValueExpression;
import com.bedrin.sloth.parser.ast.expressions.VariableExpression;
import com.bedrin.sloth.parser.ast.statements.ArrayAssignmentStatement;
import com.bedrin.sloth.parser.ast.statements.AssignmentStatement;
import com.bedrin.sloth.parser.ast.statements.BlockStatement;
import com.bedrin.sloth.parser.ast.statements.BreakStatement;
import com.bedrin.sloth.parser.ast.statements.ContinueStatement;
import com.bedrin.sloth.parser.ast.statements.DoWhileStatement;
import com.bedrin.sloth.parser.ast.statements.ForStatement;
import com.bedrin.sloth.parser.ast.statements.FunctionDefineStatement;
import com.bedrin.sloth.parser.ast.statements.FunctionalStatement;
import com.bedrin.sloth.parser.ast.statements.IfStatement;
import com.bedrin.sloth.parser.ast.statements.ReturnStatement;
import com.bedrin.sloth.parser.ast.statements.UntilStatement;
import com.bedrin.sloth.parser.ast.statements.WhileStatement;
import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IStatement;

public class Parser {
	
	private static final Token EOF = new Token(TokenType.EOF);
	
	private List<Token> tokens;
	private int size;
	private int absolutePosition;
	
	public Parser(List<Token> tokens) {
		this.tokens = tokens;
		this.size = tokens.size();
	}
	
	private boolean match(TokenType type) {
		Token current = get(0);
		if(type != current.getType()) {
			return false;
		}
		absolutePosition++;
		return true;
	}
	
	private boolean lookMatch(int offset, TokenType type) {
		Token current = get(offset);
		if(type != current.getType()) {
			return false;
		}
		return true;
	}
	
	private Token consume(TokenType type) {
		Token current = get(0);
		if(type != current.getType()) {
			throw new TokenDoesNotMatchToTypeException(current, type);
		}
		absolutePosition++;
		return current;
	}
	
	private Token get(int relativePosition) {
		int pos = this.absolutePosition + relativePosition;
		if(pos >= this.size) {
			return EOF;
		}
		return this.tokens.get(pos);
	}
	
	public IStatement parse() {
		BlockStatement result = new BlockStatement();
		while(!match(TokenType.EOF)) {
			result.add(statement());
		}
		return result;
	}
	
	private IStatement block() {
		BlockStatement block = new BlockStatement();
		consume(TokenType.FLPAREN);
		while(!match(TokenType.FRPAREN)) {
			block.add(statement());
		}
		return block;
	}
	
	private IStatement statementOrBlock() {
		if(lookMatch(0, TokenType.FLPAREN)) return block();
		return statement();
	}
	
	private IStatement statement() {
		if(match(TokenType.IF)) {
			return ifStatement();
		}
		if(match(TokenType.UNTIL)) {
			return untilStatement();
		}
		if(match(TokenType.WHILE)) {
			return whileStatement();
		}
		if(match(TokenType.DO)) {
			return doWhileStatement();
		}
		if(match(TokenType.BREAK)) {
			return new BreakStatement();
		}
		if(match(TokenType.CONTINUE)) {
			return new ContinueStatement();
		}
		if(match(TokenType.RETURN)) {
			return new ReturnStatement(arrayElementOrExpression());
		}
		if(match(TokenType.FOR)) {
			return forStatement();
		}
		if(match(TokenType.FUNCTION)) {
			return functionDefine();
		}
		if(lookMatch(0, TokenType.WORD) && lookMatch(1, TokenType.LPAREN)) {
			return new FunctionalStatement(function());
		} 
		return assignmentStatement();
	}
	
	private FunctionDefineStatement functionDefine() {
		String name = consume(TokenType.WORD).getContent();
		consume(TokenType.LPAREN);
		List<String> argList = new ArrayList<String>();
		while(!match(TokenType.RPAREN)) {
			argList.add(consume(TokenType.WORD).getContent());
			match(TokenType.COMMA);
		}
		IStatement body = statementOrBlock();
		return new FunctionDefineStatement(name, argList, body);
	}
	
	private IExpression declaratedArray() {
		consume(TokenType.QLPAREN);
		List<IExpression> elements = new ArrayList<IExpression>();
		while(!match(TokenType.QRPAREN)) {
			elements.add(arrayElementOrExpression());
			match(TokenType.COMMA);
		}
		return new ArrayExpression(elements);
	}

	private FunctionalExpression function() {
		String name = consume(TokenType.WORD).getContent();
		consume(TokenType.LPAREN);
		FunctionalExpression function = new FunctionalExpression(name);
		while(!match(TokenType.RPAREN)) {
			function.addArgument(expression());
			match(TokenType.COMMA);
		}
		return function;
	}
	
	private IExpression arrayElementOrExpression() {
		if(lookMatch(0, TokenType.WORD) && lookMatch(1, TokenType.QLPAREN)) return arrayElement();
		return expression();
	}
	
	private IExpression arrayElement() {
		String variable = consume(TokenType.WORD).getContent();
		List<IExpression> indicies = new ArrayList<IExpression>();
		do {
			consume(TokenType.QLPAREN);
			indicies.add(arrayElementOrExpression());
			consume(TokenType.QRPAREN);
		} while(lookMatch(0, TokenType.QLPAREN));
		return new ArrayAccessExpression(variable, indicies);
	}
	
	private IStatement assignmentStatement() {
		if(lookMatch(0, TokenType.WORD) && lookMatch(1, TokenType.EQ)) {
			String variable = consume(TokenType.WORD).getContent();
			consume(TokenType.EQ);
			return new AssignmentStatement(variable, arrayElementOrExpression());
		}
		if(lookMatch(0, TokenType.WORD) && lookMatch(1, TokenType.QLPAREN)) {
			ArrayAccessExpression array = (ArrayAccessExpression) arrayElement();
			consume(TokenType.EQ);
			return new ArrayAssignmentStatement(array, arrayElementOrExpression());
		}
		throw new UnknownOperatorException();
	}
	
	private IStatement ifStatement() {
		IExpression condition   = arrayElementOrExpression();
		IStatement  ifStatement = statementOrBlock();
		IStatement  elseStatement;
		if(match(TokenType.ELSE)) {
			elseStatement = statementOrBlock();
		} else {
			elseStatement = null;
		}
		return new IfStatement(condition, ifStatement, elseStatement);
	}
	
	private IStatement untilStatement() {
		IExpression condition   = arrayElementOrExpression();
		IStatement  untilStatement = statementOrBlock();
		IStatement  elseStatement;
		if(match(TokenType.ELSE)) {
			elseStatement = statementOrBlock();
		} else {
			elseStatement = null;
		}
		return new UntilStatement(condition, untilStatement, elseStatement);
	}
	
	private IStatement whileStatement() {
		IExpression condition = arrayElementOrExpression();
		IStatement  statement = statementOrBlock();
		return new WhileStatement(condition, statement);
	}
	
	private IStatement forStatement() {
		IStatement  initialization = assignmentStatement();
		consume(TokenType.COMMA);
		IExpression termination = arrayElementOrExpression();
		consume(TokenType.COMMA);
		IStatement  decIncrementation = assignmentStatement();
		IStatement  statement = statementOrBlock();
		return new ForStatement(initialization, termination, decIncrementation, statement)	;
	}
	
	private IStatement doWhileStatement() {
		IStatement  statement = statementOrBlock();
		consume(TokenType.WHILE);
		IExpression condition = arrayElementOrExpression();
		return new DoWhileStatement(condition, statement);
	}
	
	private IExpression expression() {
		return logicalOr();
	}
	
	private IExpression logicalOr() {
		IExpression result = logicalAnd();
		while (true) {
			if (match(TokenType.OR)) {
				result = new ConditionalExpression(ConditionalExpression.Operator.OR, result, logicalAnd());
				continue;
			} 
			break;
		}
		return result;
	}
	
	private IExpression logicalAnd() {
		IExpression result = equality();
		while (true) {
			if (match(TokenType.AND)) {
				result = new ConditionalExpression(ConditionalExpression.Operator.AND, result, equality());
				continue;
			} 
			break;
		}
		return result;
	}
	
	private IExpression equality() {
		IExpression result = conditional();
		if(match(TokenType.EQEQ)) {
			return new ConditionalExpression(ConditionalExpression.Operator.EQUALS, result, conditional());
		} 
		if(match(TokenType.EXCLEQ)) {
			return new ConditionalExpression(ConditionalExpression.Operator.NOT_EQUALS, result, conditional());
		}
		return result;
	}
	
	private IExpression conditional() {
		IExpression result = additive();
		while(true) {
			if(match(TokenType.LT)) {
				result = new ConditionalExpression(ConditionalExpression.Operator.LT, result, additive());
				continue;
			}
			if(match(TokenType.LTEQ)) {
				result = new ConditionalExpression(ConditionalExpression.Operator.LTEQ, result, additive());
				continue;
			}
			if(match(TokenType.GT)) {
				result = new ConditionalExpression(ConditionalExpression.Operator.GT, result, additive());
				continue;
			}
			if(match(TokenType.GTEQ)) {
				result = new ConditionalExpression(ConditionalExpression.Operator.GTEQ, result, additive());
				continue;
			}
			break;
		}
		return result;
	}
	
	private IExpression additive() {
		IExpression result = multiplicative();
		while(true) {
			if(match(TokenType.PLUS)) {
				result = new BinaryExpression(BinaryExpression.Operator.PLUS, result, multiplicative());
				continue;
			} 
			if(match(TokenType.MINUS)) {
				result = new BinaryExpression(BinaryExpression.Operator.MINUS, result, multiplicative());
				continue;
			}
			break;
		}
		return result;
	}
	
	private IExpression multiplicative() {
		IExpression result = bitive();
		while(true) {
			if(match(TokenType.POWER)) {
				result = new BinaryExpression(BinaryExpression.Operator.POWER, result, bitive());
				continue;
			}
			if(match(TokenType.STAR)) {
				result = new BinaryExpression(BinaryExpression.Operator.MULTIPLY, result, bitive());
				continue;
			} 
			if(match(TokenType.SLASH)) {
				result = new BinaryExpression(BinaryExpression.Operator.DIVIDE, result, bitive());
				continue;
			}
			break;
		}
		return result;
	}
	
	private IExpression bitive() {
		IExpression result = unary();
		while(true) {
			if(match(TokenType.RTRT)) {
				result = new BinaryExpression(BinaryExpression.Operator.RIGHT_SHIFT, result, unary());
				continue;
			}
			if(match(TokenType.LTLT)) {
				result = new BinaryExpression(BinaryExpression.Operator.LEFT_SHIFT, result, unary());
				continue;
			}
			if(match(TokenType.RTRTRT)) {
				result = new BinaryExpression(BinaryExpression.Operator.RIGHT_SHIFT_WITHOUT_MARK, result, unary());
				continue;
			}
			if(match(TokenType.LTLTLT)) {
				result = new BinaryExpression(BinaryExpression.Operator.LEFT_SHIFT_WITHOUT_MARK, result, unary());
				continue;
			}
			if(match(TokenType.BAREXCL)) {
				result = new BinaryExpression(BinaryExpression.Operator.XOR, result, unary());
				continue;
			}
			if(match(TokenType.AMP)) {
				result = new BinaryExpression(BinaryExpression.Operator.AND, result, unary());
				continue;
			}
			if(match(TokenType.BAR)) {
				result = new BinaryExpression(BinaryExpression.Operator.OR, result, unary());
				continue;
			}
			break;
		}
		return result;
	}
	
	private IExpression unary() {
		if(match(TokenType.MINUS)) {
			return new UnaryExpression(UnaryExpression.Operator.MINUS, primary());
		}
		if(match(TokenType.PLUS)) {
			return primary();
		}
		if(match(TokenType.FACT)) {
			return new UnaryExpression(UnaryExpression.Operator.FACTORIAL, primary());
		}
		if(match(TokenType.INVERSION)) {
			return new UnaryExpression(UnaryExpression.Operator.INVERSION, primary());
		}
		return primary();
	}
	
	private IExpression primary() {
		Token current = get(0);
		if(match(TokenType.NUMBER)) {
			return new ValueExpression(Double.parseDouble(current.getContent()));
		}
		if(match(TokenType.HEX_NUMBER)) {
			return new ValueExpression(Long.parseLong(current.getContent(), 16));
		}
		if(match(TokenType.BIT_CHAIN)) {
			return new ValueExpression(Integer.parseInt(current.getContent(), 2));
		}
		if(match(TokenType.LPAREN)) {
			IExpression result = arrayElementOrExpression();
			match(TokenType.RPAREN);
			return result;
		}
		if(lookMatch(0, TokenType.WORD) && lookMatch(1, TokenType.LPAREN)) {
			return function();
		}
		if(lookMatch(0, TokenType.WORD) && lookMatch(1, TokenType.QLPAREN)) {
			return arrayElement();
		}
		if(lookMatch(0, TokenType.QLPAREN)) {
			return declaratedArray();
		}
		if(match(TokenType.WORD)) {
			return new VariableExpression(current.getContent());
		}
		if(match(TokenType.TEXT)) {
			return new ValueExpression(current.getContent());
		}
		throw new UnknownExpressionException();
	}

}
