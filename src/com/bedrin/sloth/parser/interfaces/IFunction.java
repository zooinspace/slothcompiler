package com.bedrin.sloth.parser.interfaces;

public interface IFunction {
	IValue execute(IValue... args);
}
