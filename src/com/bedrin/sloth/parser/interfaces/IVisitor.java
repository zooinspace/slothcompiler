package com.bedrin.sloth.parser.interfaces;

import com.bedrin.sloth.parser.ast.expressions.ArrayAccessExpression;
import com.bedrin.sloth.parser.ast.expressions.ArrayExpression;
import com.bedrin.sloth.parser.ast.expressions.BinaryExpression;
import com.bedrin.sloth.parser.ast.expressions.ConditionalExpression;
import com.bedrin.sloth.parser.ast.expressions.FunctionalExpression;
import com.bedrin.sloth.parser.ast.expressions.UnaryExpression;
import com.bedrin.sloth.parser.ast.expressions.ValueExpression;
import com.bedrin.sloth.parser.ast.expressions.VariableExpression;
import com.bedrin.sloth.parser.ast.statements.ArrayAssignmentStatement;
import com.bedrin.sloth.parser.ast.statements.AssignmentStatement;
import com.bedrin.sloth.parser.ast.statements.BlockStatement;
import com.bedrin.sloth.parser.ast.statements.BreakStatement;
import com.bedrin.sloth.parser.ast.statements.ContinueStatement;
import com.bedrin.sloth.parser.ast.statements.DoWhileStatement;
import com.bedrin.sloth.parser.ast.statements.ForStatement;
import com.bedrin.sloth.parser.ast.statements.FunctionDefineStatement;
import com.bedrin.sloth.parser.ast.statements.FunctionalStatement;
import com.bedrin.sloth.parser.ast.statements.IfStatement;
import com.bedrin.sloth.parser.ast.statements.ReturnStatement;
import com.bedrin.sloth.parser.ast.statements.UntilStatement;
import com.bedrin.sloth.parser.ast.statements.WhileStatement;

public interface IVisitor {
	
	void visit(AssignmentStatement st);
	void visit(FunctionDefineStatement st);
	void visit(FunctionalStatement st);
	void visit(ArrayAccessExpression arrayAccessExpression);
	void visit(ArrayExpression arrayExpression);
	void visit(BinaryExpression binaryExpression);
	void visit(ConditionalExpression conditionalExpression);
	void visit(FunctionalExpression functionalExpression);
	void visit(UnaryExpression unaryExpression);
	void visit(ValueExpression valueExpression);
	void visit(VariableExpression variableExpression);
	void visit(ArrayAssignmentStatement arrayAssignmentStatement);
	void visit(BlockStatement blockStatement);
	void visit(BreakStatement breakStatement);
	void visit(ContinueStatement continueStatement);
	void visit(DoWhileStatement doWhileStatement);
	void visit(ForStatement forStatement);
	void visit(IfStatement ifStatement);
	void visit(ReturnStatement returnStatement);
	void visit(UntilStatement untilStatement);
	void visit(WhileStatement whileStatement);
	
}
