package com.bedrin.sloth.parser.interfaces;

public interface IExpression extends INode {
	IValue eval();
}
