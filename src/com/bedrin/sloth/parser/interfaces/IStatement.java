package com.bedrin.sloth.parser.interfaces;

public interface IStatement extends INode {
	void execute();
}
