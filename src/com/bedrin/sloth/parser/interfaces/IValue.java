package com.bedrin.sloth.parser.interfaces;

public interface IValue {
	double asDouble();
	String asString();
}
