package com.bedrin.sloth.parser.interfaces;

public interface INode {
	void accept(IVisitor visitor);
}
