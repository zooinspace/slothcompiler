package com.bedrin.sloth.parser.ast.expressions;

import java.util.List;

import com.bedrin.sloth.datatypes.ArrayValue;
import com.bedrin.sloth.exceptions.WrongArrayCallException;
import com.bedrin.sloth.memory.Variables;
import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IValue;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class ArrayAccessExpression implements IExpression {

	private String variable;
	private List<IExpression> indicies;
	
	public ArrayAccessExpression(String variable, List<IExpression> indicies) {
		this.variable = variable;
		this.indicies = indicies;
	}
	
	@Override
	public IValue eval() {
		return getArray().get(lastIndex());
	}
	
	public ArrayValue getArray() {
		ArrayValue array = consumeArray(Variables.get(this.variable));
		int last = indicies.size() - 1;
		for (int i = 0; i < last; i++) {
			array = consumeArray(array.get(index(i)));
		}
		return array;
	}
	
	public int lastIndex() {
		return index(indicies.size() - 1);
	}
	
	private int index(int i) {
		return (int) indicies.get(i).eval().asDouble();
	}
	
	private ArrayValue consumeArray(IValue value) {
		if(value instanceof ArrayValue) {
			return (ArrayValue) value;
		} else {
			throw new WrongArrayCallException();
		}
	}
	
	public String getVariable() {
		return variable;
	}
	
	public List<IExpression> getIndicies() {
		return indicies;
	}
	
	@Override
	public String toString() {
		return this.variable + this.indicies;
	}

	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
	
}
