package com.bedrin.sloth.parser.ast.expressions;

import com.bedrin.sloth.memory.Variables;
import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IValue;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class VariableExpression implements IExpression {

	private final String name;
	
	public VariableExpression(String name) {
		this.name = name;
	}
	
	@Override
	public IValue eval() {
		return Variables.get(name);
	}
	
	@Override
	public String toString() {
		return String.format("[%s:%s]", name, Variables.get(name));
	}
	

	public String getName() {
		return name;
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
	
}
