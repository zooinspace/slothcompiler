package com.bedrin.sloth.parser.ast.expressions;

import java.util.ArrayList;
import java.util.List;

import com.bedrin.sloth.datatypes.UserDefinedFunction;
import com.bedrin.sloth.exceptions.WrongNumberOfArgumentsException;
import com.bedrin.sloth.memory.Functions;
import com.bedrin.sloth.memory.Variables;
import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IFunction;
import com.bedrin.sloth.parser.interfaces.IValue;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class FunctionalExpression implements IExpression {

	private String name;
	private List<IExpression> arguments;
	
	public FunctionalExpression(String name, List<IExpression> arguments) {
		this.name = name;
		this.arguments = arguments;
	}
	
	public FunctionalExpression(String name) {
		this.name = name;
		this.arguments = new ArrayList<IExpression>();
	}
	
	public void addArgument(IExpression arg) {
		this.arguments.add(arg);
	}
	
	@Override
	public IValue eval() {
		int size = this.arguments.size();
		IValue[] values = new IValue[size];
		for (int i = 0; i < size; i++) {
			values[i] = arguments.get(i).eval();
		}
		IFunction function = Functions.get(name);
		if(function instanceof UserDefinedFunction) {
			UserDefinedFunction userFunction = (UserDefinedFunction) function;
			if(size != userFunction.getArgsCount()) {
				throw new WrongNumberOfArgumentsException(this.name, size);
			}
			Variables.push();
			for (int i = 0; i < size; i++) {
				Variables.set(userFunction.getArgNameByIndex(i), values[i]);
			}
			IValue result = function.execute(values);
			Variables.pop();
			return result;
		}
		return function.execute(values);
	}
	
	@Override
	public String toString() {
		return this.name + "(" + this.arguments.toString() + ")";
	}

	public List<IExpression> getArguments() {
		return arguments;
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
	
}
