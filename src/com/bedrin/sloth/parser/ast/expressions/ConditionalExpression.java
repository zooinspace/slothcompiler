package com.bedrin.sloth.parser.ast.expressions;

import com.bedrin.sloth.datatypes.NumberValue;
import com.bedrin.sloth.datatypes.StringValue;
import com.bedrin.sloth.exceptions.UnknownOperatorException;
import com.bedrin.sloth.exceptions.WrongComparisonException;
import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IValue;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class ConditionalExpression implements IExpression {
	
	public static enum Operator {
			PLUS("+"),
			MINUS("-"),
			MULTIPLY("*"),
			DIVIDE("/"),
			
			EQUALS("=="),
			NOT_EQUALS("!="),
			
			LT("<"),
			GT(">"),
			LTEQ("<="),
			GTEQ(">="),
			
			AND("&&"),
			OR("||");
		
		private final String name;

		public String getName() {
			return name;
		}
		
		private Operator(String name) {
			this.name = name;
		}
	};
	
	private IExpression first, second;
	private Operator operation;
	
	public ConditionalExpression(Operator operation, IExpression first, IExpression second) {
		this.operation = operation;
		this.first = first;
		this.second = second;
	}
	
	@Override
	public String toString() {
		return String.format("(%s %s %s)", this.first, this.operation.getName(), this.second);
	}
	
	@Override
	public IValue eval() {
		
		IValue n1 = this.first.eval();
		IValue n2 = this.second.eval();
		
		double number1, number2;
		if(n1 instanceof StringValue && n2 instanceof StringValue) {
			number1 = n1.asString().compareTo(n2.asString());
			number2 = 0;
		} else if(n1 instanceof NumberValue && n2 instanceof NumberValue) {
			number1 = n1.asDouble();
			number2 = n2.asDouble();
		} else {
			throw new WrongComparisonException("Number", "string");
		}
		boolean result;
		switch(operation) {
		
			case LT:     result = number1 <  number2; break;
			case LTEQ:   result = number1 <= number2; break;
			case GT:     result = number1 >  number2; break;
			case GTEQ:   result = number1 >= number2; break;
			case EQUALS: result = number1 == number2; break;
			
			case AND:    result = (number1 != 0) && (number2 != 0); break;
			case OR:     result = (number1 != 0) || (number2 != 0); break;

			default: throw new UnknownOperatorException();
		}
		return new NumberValue(result);
	}
	
	public IExpression getFirst() {
		return first;
	}
	
	public IExpression getSecond() {
		return second;
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
}
