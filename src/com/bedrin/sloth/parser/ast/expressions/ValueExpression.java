package com.bedrin.sloth.parser.ast.expressions;

import com.bedrin.sloth.datatypes.NumberValue;
import com.bedrin.sloth.datatypes.StringValue;
import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IValue;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class ValueExpression implements IExpression {

	private final IValue value;
	
	public ValueExpression(double value) {
		this.value = new NumberValue(value);
	}
	
	public ValueExpression(String value) {
		this.value = new StringValue(value);
	}
	
	@Override
	public String toString() {
		return this.value.asString();
	}
	
	@Override
	public IValue eval() {
		return this.value;
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}

}
