package com.bedrin.sloth.parser.ast.expressions;

import java.util.List;

import com.bedrin.sloth.datatypes.ArrayValue;
import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IValue;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class ArrayExpression implements IExpression {

	private List<IExpression> elements;
	
	public ArrayExpression(List<IExpression> elements) {
		this.elements = elements;
	}
	
	public void addArgument(IExpression arg) {
		this.elements.add(arg);
	}
	
	@Override
	public IValue eval() {
		int size = this.elements.size();
		ArrayValue arr = new ArrayValue(size);
		for(int i = 0; i < size; i++) {
			arr.set(i, this.elements.get(i).eval());
		}
		return arr;
	}
	
	@Override
	public String toString() {
		return this.elements.toString();
	}

	public List<IExpression> getElements() {
		return elements;
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
	
}
