package com.bedrin.sloth.parser.ast.expressions;

import com.bedrin.sloth.datatypes.NumberValue;
import com.bedrin.sloth.datatypes.StringValue;
import com.bedrin.sloth.exceptions.UnknownOperatorException;
import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IValue;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class UnaryExpression implements IExpression {

	public static enum Operator {
		FACTORIAL("$"),
		MINUS("-"),
		INVERSION("~");
		
		private final String name;
		
		public String getName() {
			return name;
		}
		
		private Operator(String name) {
			this.name = name;
		}
	}
	
	private IExpression first;
	private Operator operation;
	
	public UnaryExpression(Operator operation, IExpression first) {
		this.operation = operation;
		this.first = first;
	}
	
	public IValue eval() {
		IValue n = this.first.eval();
		if(n instanceof StringValue) {
			switch(operation) {
				case MINUS: return new StringValue(new StringBuffer(n.asString()).reverse().toString());
				case FACTORIAL: return new NumberValue(FactTree(n.asString().length()));
				default:  return this.first.eval();
			}
		} else {
				switch(operation) {
				case MINUS: return new NumberValue(-n.asDouble());
				case FACTORIAL: if(n.asDouble() < 0) throw new UnknownOperatorException();
					return new NumberValue(FactTree((long) Math.floor(n.asDouble())));
				case INVERSION: return new NumberValue(~(long) n.asDouble());
				default:  return this.first.eval();
			}
		}
		
	}
	
	@Override
	public String toString() {
		return String.format("%s%s", operation.getName(), first);
	}
	
	private static long ProdTree(long l, long r) {
	    if (l > r)
	        return 1;
	    if (l == r)
	         return l;
	    if (r - l == 1)
	        return l * r;
	    long m = (long) (l + r) / 2;
	    return ProdTree(l, m) * ProdTree(m + 1, r);
	}
	        
	private static long FactTree(long n) {
	    if (n < 0)
	        return 0;
	    if (n == 0)
	        return 1;
	    if (n == 1 || n == 2)
	        return n;
	    return ProdTree(2, n);
	}       

	public IExpression getFirst() {
		return first;
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
	
}
