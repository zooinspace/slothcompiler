package com.bedrin.sloth.parser.ast.expressions;

import com.bedrin.sloth.datatypes.ArrayValue;
import com.bedrin.sloth.datatypes.NumberValue;
import com.bedrin.sloth.datatypes.StringValue;
import com.bedrin.sloth.exceptions.UnknownOperatorException;
import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IValue;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class BinaryExpression implements IExpression {

	public static enum Operator {
		
		PLUS("+"),
		MINUS("-"),
		MULTIPLY("*"),
		DIVIDE("/"),
		POWER("^"),
		
		LEFT_SHIFT("<<"),
		RIGHT_SHIFT("<<"),
		LEFT_SHIFT_WITHOUT_MARK("<<<"),
		RIGHT_SHIFT_WITHOUT_MARK(">>>"),
		AND("&"),
		OR("|"),
		XOR("|!");
	
		private final String name;
	
		public String getName() {
			return name;
		}
		
		private Operator(String name) {
			this.name = name;
		}
	};
	
	private IExpression first, second;
	private Operator operation;
	
	public BinaryExpression(Operator operation, IExpression first, IExpression second) {
		this.operation = operation;
		this.first = first;
		this.second = second;
	}
	
	@Override
	public String toString() {
		return String.format("(%s %s %s)", this.first, this.operation.getName(), this.second);
	}
	
	@Override
	public IValue eval() {
		IValue n1 = this.first.eval();
		IValue n2 = this.second.eval();
		if(n1 instanceof StringValue || n2 instanceof StringValue 
				|| n1 instanceof ArrayValue || n2 instanceof ArrayValue) {
			switch(operation) {
				case PLUS: return new StringValue(n1.asString() + n2.asString());
				case MULTIPLY: 
					if(n2 instanceof StringValue && !(n1 instanceof StringValue)) {
						StringBuilder buffer = new StringBuilder();
						for(int i = 0; i < n1.asDouble(); i++) {
							buffer.append(n2.asString());
						}
						return new StringValue(buffer.toString());
					} else if(n1 instanceof StringValue && !(n2 instanceof StringValue)) {
						StringBuilder buffer = new StringBuilder();
						for(int i = 0; i < n2.asDouble(); i++) {
							buffer.append(n1.asString());
						}
						return new StringValue(buffer.toString());
					} else {
						throw new UnknownOperatorException();
					}
					
				default:  throw new UnknownOperatorException();
			}
		} else {
			switch(operation) {
				case PLUS: return new NumberValue(n1.asDouble() + n2.asDouble());
				case MINUS: return new NumberValue(n1.asDouble() - n2.asDouble());
				case MULTIPLY: return new NumberValue(n1.asDouble() * n2.asDouble());
				case DIVIDE: return new NumberValue(n1.asDouble() / n2.asDouble());
				case POWER: return new NumberValue(Math.pow(n1.asDouble(), n2.asDouble()));
				case LEFT_SHIFT: return new NumberValue((long) n1.asDouble() << (long) n2.asDouble());
				case RIGHT_SHIFT: return new NumberValue((long) n1.asDouble() >> (long) n2.asDouble());
				case RIGHT_SHIFT_WITHOUT_MARK: return new NumberValue((long) n1.asDouble() >>> (long) n2.asDouble());
				case LEFT_SHIFT_WITHOUT_MARK: 
					StringBuffer temp = new StringBuffer(Long.toBinaryString((long) n1.asDouble()));
					long iterate = (long) n2.asDouble();
					for(byte i = 0; i < iterate; i++) {
						temp.deleteCharAt(i);
						temp.append('0');
					}
					long temp2  = 0;
					if(temp.charAt(0) == '1') {
						for(int i = 0; i < temp.length(); i++) {
							if(temp.charAt(i) == '0') {
								temp.setCharAt(i, '1');
							} else if(temp.charAt(i) == '1') {
								temp.setCharAt(i, '0');
							}
						}
						temp2 = -(Long.parseLong(temp.toString(), 2) + 1);
						return new NumberValue(temp2);
					} else {
						return new NumberValue(Long.parseLong(temp.toString(), 2));
					}
				case XOR: return new NumberValue((long) n1.asDouble() ^ (long) n2.asDouble());
				case AND: return new NumberValue((long) n1.asDouble() & (long) n2.asDouble());
				case OR: return new NumberValue((long) n1.asDouble() | (long) n2.asDouble());
				
				default:  throw new UnknownOperatorException();
			}
		}
	}
	
	public IExpression getFirst() {
		return first;
	}
	
	public IExpression getSecond() {
		return second;
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
}
