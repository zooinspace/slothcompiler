package com.bedrin.sloth.parser.ast.statements;

import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class IfStatement implements IStatement {

	private IExpression expression;
	private IStatement ifStatement, elseStatement;
	
	public IfStatement(IExpression expression, IStatement ifStatement, IStatement elseStatement) {
		this.expression = expression;
		this.ifStatement = ifStatement;
		this.elseStatement = elseStatement;
	}
	
	@Override
	public void execute() {
		double result = this.expression.eval().asDouble();
		if(result != 0) {
			this.ifStatement.execute();
		} else if(this.elseStatement != null) {
			this.elseStatement.execute();
		}
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("if ").append(this.expression).append(" ").append(this.ifStatement);
		if(this.elseStatement != null) {
			result.append("\nelse").append(this.elseStatement);
		}
		return result.toString();
	}
	
	public IStatement getElseStatement() {
		return elseStatement;
	}
	
	public IExpression getExpression() {
		return expression;
	}
	
	public IStatement getIfStatement() {
		return ifStatement;
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
	
}
