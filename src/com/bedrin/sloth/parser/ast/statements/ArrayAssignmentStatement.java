package com.bedrin.sloth.parser.ast.statements;

import com.bedrin.sloth.parser.ast.expressions.ArrayAccessExpression;
import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class ArrayAssignmentStatement implements IStatement {

	private ArrayAccessExpression array;
	private IExpression expression;
	
	public ArrayAssignmentStatement(ArrayAccessExpression array, IExpression expression) {
		this.array = array;
		this.expression = expression;
	}
	
	@Override
	public void execute() {
		array.getArray().set(array.lastIndex(), expression.eval());
	}
	
	@Override
	public String toString() {
		return String.format("%s = %s" , this.array, this.expression);
	}
	
	public ArrayAccessExpression getArray() {
		return array;
	}
	
	public IExpression getExpression() {
		return expression;
	}

	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
	
}
