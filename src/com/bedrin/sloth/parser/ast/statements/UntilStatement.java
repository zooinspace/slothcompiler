package com.bedrin.sloth.parser.ast.statements;

import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class UntilStatement implements IStatement {
	
	private IExpression expression;
	private IStatement untilStatement, elseStatement;
	
	public UntilStatement(IExpression expression, IStatement untilStatement, IStatement elseStatement) {
		this.expression = expression;
		this.untilStatement = untilStatement;
		this.elseStatement = elseStatement;
	}
	
	@Override
	public void execute() {
		double result = this.expression.eval().asDouble();
		if(result == 0) {
			this.untilStatement.execute();
		} else if(this.elseStatement != null) {
			this.elseStatement.execute();
		}
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("until ").append(this.expression).append(" ").append(this.untilStatement);
		if(this.elseStatement != null) {
			result.append("\nelse").append(this.elseStatement);
		}
		return result.toString();
	}
	
	public IStatement getElseStatement() {
		return elseStatement;
	}
	
	public IExpression getExpression() {
		return expression;
	}
	
	public IStatement getUntilStatement() {
		return untilStatement;
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
}
