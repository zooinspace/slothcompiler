package com.bedrin.sloth.parser.ast.statements;

import com.bedrin.sloth.memory.Variables;
import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class AssignmentStatement implements IStatement {

	private String variable;
	private IExpression expression;
	
	public AssignmentStatement(String variable, IExpression expression) {
		this.expression = expression;
		this.variable = variable;
	}

	@Override
	public void execute() {
		Variables.set(this.variable, this.expression.eval());
	}
	
	@Override
	public String toString() {
		return String.format("%s = %s", this.variable, this.expression);
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}

	public String getVariable() {
		return variable;
	}
	
	public IExpression getExpression() {
		return expression;
	}
	
	public void setExpression(IExpression expression) {
		this.expression = expression;
	}
	
	public void setVariable(String variable) {
		this.variable = variable;
	}
	
}
