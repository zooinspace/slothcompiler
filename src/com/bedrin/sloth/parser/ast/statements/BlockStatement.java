package com.bedrin.sloth.parser.ast.statements;

import java.util.ArrayList;
import java.util.List;

import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class BlockStatement implements IStatement {

	private List<IStatement> statements;
	
	public BlockStatement() {
		statements = new ArrayList<IStatement>();
	}
	
	public void add(IStatement statement) {
		this.statements.add(statement);
	}
	
	@Override
	public void execute() {
		this.statements.forEach((statement) -> statement.execute());
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		this.statements.forEach((statement) -> result.append(statement.toString()).append(System.lineSeparator()));
		return "{" + result.toString() + "}";
	}
	
	public List<IStatement> getStatements() {
		return statements;
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
	
}
