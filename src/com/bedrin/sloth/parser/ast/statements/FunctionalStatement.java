package com.bedrin.sloth.parser.ast.statements;

import com.bedrin.sloth.parser.ast.expressions.FunctionalExpression;
import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class FunctionalStatement implements IStatement {

	private FunctionalExpression function;
	
	public FunctionalStatement(FunctionalExpression function) {
		this.function = function;
	}

	@Override
	public void execute() {
		this.function.eval();
	}
	
	public FunctionalExpression getFunction() {
		return function;
	}
	
	@Override
	public String toString() {
		return this.function.toString();
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}

}
