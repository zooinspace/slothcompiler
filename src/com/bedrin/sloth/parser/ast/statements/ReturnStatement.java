package com.bedrin.sloth.parser.ast.statements;

import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IValue;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class ReturnStatement extends RuntimeException implements IStatement {

	private static final long serialVersionUID = -3381721796160945947L;
	private IExpression expression;
	private IValue result;
	
	public ReturnStatement(IExpression expression) {
		this.expression = expression;
	}
	
	@Override
	public void execute() {
		this.result = this.expression.eval();
		throw this;
	}
	
	@Override
	public String toString() {
		return "return";
	}

	public IValue getResult() {
		return result;
	}

	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
	
}
