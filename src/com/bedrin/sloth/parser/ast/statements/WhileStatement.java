package com.bedrin.sloth.parser.ast.statements;

import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class WhileStatement implements IStatement {

	private IExpression condition;
	private IStatement statement;

	public WhileStatement(IExpression condition, IStatement statement) {
		this.condition = condition;
		this.statement = statement;
	}
	
	@Override
	public void execute() {
		while (this.condition.eval().asDouble() != 0) {
			try {
				this.statement.execute();
			} catch (BreakStatement b) {
				break;
			} catch (ContinueStatement c) {
				continue;
			}
		}
	}
	
	@Override
	public String toString() {
		return "while (" + this.condition + ") {" + this.statement + "}";
	}
	
	public IExpression getCondition() {
		return condition;
	}
	
	public IStatement getStatement() {
		return statement;
	}

	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
	
}
