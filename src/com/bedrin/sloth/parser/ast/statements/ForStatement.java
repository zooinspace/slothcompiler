package com.bedrin.sloth.parser.ast.statements;

import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class ForStatement implements IStatement {
	
	private IStatement  initialization;
	private IExpression termination;
	private IStatement  decIncrementation;
	private IStatement 	statement;
	
	public ForStatement(IStatement initialization, IExpression termination, IStatement decIncrementation, IStatement statement) {
		this.initialization = initialization;
		this.termination = termination;
		this.decIncrementation = decIncrementation;
		this.statement = statement;
	}
	
	@Override
	public void execute() {
		for (this.initialization.execute(); this.termination.eval().asDouble() != 0; this.decIncrementation.execute()) {
			try {
				this.statement.execute();
			} catch (BreakStatement b) {
				break;
			} catch (ContinueStatement c) {
				continue;
			}
		}
	}
	
	@Override
	public String toString() {
		return "for (" + this.initialization + ", " + this.termination + ", " + this.decIncrementation + ") {" + this.statement + "}";
	}
	
	public IStatement getDecIncrementation() {
		return decIncrementation;
	}
	
	public IStatement getInitialization() {
		return initialization;
	}
	
	public IStatement getStatement() {
		return statement;
	}
	
	public IExpression getTermination() {
		return termination;
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
	
}
