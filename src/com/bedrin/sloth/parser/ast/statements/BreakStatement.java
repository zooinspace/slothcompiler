package com.bedrin.sloth.parser.ast.statements;

import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class BreakStatement extends RuntimeException implements IStatement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2494586152528634702L;

	@Override
	public void execute() {
		throw this;
	}
	
	@Override
	public String toString() {
		return "break";
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}

}
