package com.bedrin.sloth.parser.ast.statements;

import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class ContinueStatement extends RuntimeException implements IStatement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8238791819906526503L;

	@Override
	public void execute() {
		throw this;
	}
	
	@Override
	public String toString() {
		return "continue";
	}

	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
	
}
