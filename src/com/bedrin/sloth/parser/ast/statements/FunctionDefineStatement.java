package com.bedrin.sloth.parser.ast.statements;

import java.util.List;

import com.bedrin.sloth.datatypes.UserDefinedFunction;
import com.bedrin.sloth.memory.Functions;
import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class FunctionDefineStatement implements IStatement {

	private String name;
	private List<String> argList;
	private IStatement body;
	
	public FunctionDefineStatement(String name, List<String> argList, IStatement body) {
		this.name = name;
		this.argList = argList;
		this.body = body;
	}
	
	@Override
	public void execute() {
		Functions.set(name, new UserDefinedFunction(this.argList, this.body));
	}
	
	@Override
	public String toString() {
		return "function(" + this.argList.toString() + ") {" + this.body.toString() + "}";
	}

	public IStatement getBody() {
		return body;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
	
}
