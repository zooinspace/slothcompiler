package com.bedrin.sloth.parser.ast.statements;

import com.bedrin.sloth.parser.interfaces.IExpression;
import com.bedrin.sloth.parser.interfaces.IStatement;
import com.bedrin.sloth.parser.interfaces.IVisitor;

public class DoWhileStatement implements IStatement {

	private IExpression condition;
	private IStatement  statement;
	
	public DoWhileStatement(IExpression condition, IStatement statement) {
		this.condition = condition;
		this.statement = statement;
	}
	
	@Override
	public void execute() {
		do {
			try {
				statement.execute();
			} catch (BreakStatement b) {
				break;
			} catch (ContinueStatement c) {
				continue;
			}
		} while (condition.eval().asDouble() != 0);
	}

	@Override
	public String toString() {
		return "do {" + this.statement + "} while (" + condition + ")";
	}
	
	public IExpression getCondition() {
		return condition;
	}
	
	public IStatement getStatement() {
		return statement;
	}
	
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
	
}
