package com.bedrin.sloth.parser;

public class Token {

	private String content;
	private TokenType type;
	
	public Token() {}
	
	public Token(String content, TokenType type) {
		this.content = content;
		this.type = type;
	}

	public Token(TokenType type) {
		this.type = type;
		this.content = "";
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public TokenType getType() {
		return type;
	}

	public void setType(TokenType type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return  !this.content.isEmpty() ? this.type + " : " + this.content : this.type + "";
	}
	
}
